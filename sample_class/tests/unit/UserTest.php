<?php 

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
	 * Test success
	 * This funtion will pass test
	 */
    
    public function testUser(){
        $user = new App\Models\User;

        $user->setUser('khang');

        $this->assertEquals($user->getUser(), 'khang');
    }

    public function testAuthenticate(){
        $user = new App\Models\User;

        $user->setUser('khang');
        $user->setPass('123123');

        $this->assertTrue($user->authorize('khang', '123123'));
    }

    /**
	 * Not a test function
	 * This funtion will not running cause it doesn't have "test" suffix
	 */
    
    public function login(){    
        $user = new App\Models\User;
        $this->assertTrue(false);
    }

}

?>