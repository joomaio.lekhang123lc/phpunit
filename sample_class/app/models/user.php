<?php 

namespace App\Models;

class User
{

    public $user;

    public $pass;

    public $email;

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setPass($pass)
    {
        $this->pass = md5($pass);
    }

    public function getPass()
    {
        return $this->pass;
    }

    // A function error because you have to encrypt md5 $pass before check
    public function authorize($username, $password)
    {
        return ( $username == $this->user && md5($password) == $this->pass );
    }
}

?>