<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TimeController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function getTimes(Request $request)
    {
        $data  = $this->validateData($request);
        $where = [];
        if ( !empty($data['from']) ) $where[] = [ 'start_time', '>=', $data['from'] ];
        if ( !empty($data['to'])   ) $where[] = [ 'end_time', '<=', $data['to'] ];
        if ( !empty($data['uid'])  ) $where[] = [ 'available_time.uid', '=', $data['uid'] ];

        $res = [];
        $times = DB::table('available_time')
                    ->selectRaw('available_time.id, available_time.uid, available_time.status,
                            DATE_FORMAT(start_time, "%H:%i:%s %d/%m/%Y" ) as start_time, 
                            DATE_FORMAT(end_time, "%H:%i:%s %d/%m/%Y" ) as end_time, 
                            DATE_FORMAT(modified_at, "%H:%i:%s %d/%m/%Y" ) as modified_at, 
                            user.name')
                    ->leftJoin('user', 'user.id', '=', 'available_time.uid')
                    ->where($where)->orderByRaw('start_time ASC')->get();
        
        $res['data'] = $times;
        return response()->json($res, 200);
    }

    protected function validateData($request){
        $data  = $request->except('start_time', 'end_time', 'modified_at');
        $times = $request->only('start_time', 'end_time', 'from', 'to');

        foreach($times as $key => $time){
            if($key == 'to') $time.=" 23:59:59";
            $timedate   = new Carbon( str_replace("/", "-", $time) );
            $data[$key] = $timedate->toDateTimeString();
        }
        $now   = Carbon::now('Asia/Ho_Chi_Minh');
        $data['modified_at'] = $now->toDateTimeString();

        return $data;
    }

    public function addTime(Request $request)
    {
        $data = $this->validateData($request);
        $res = [];
        $res['id'] = DB::table('available_time')->insertGetId($data);
        if($res['id']){
            $res['msg'] = "commit time successfully!";
            return response()->json($res, 201);
        }
        else
        {
            return response()->json("Can not create");
        }
    }

    public function editTime($id, Request $request)
    {
        $data = $this->validateData($request);
        
        if(DB::table('available_time')->where('id',$id)->update($data)){
            $time = DB::table('available_time')->find($id);

            $res = [];
            $res['msg'] = "update time successfully";
            $res['time'] = $time;
            return response()->json($res,200);
        }
        else
        {
            return response()->json("Can not update");
        }
    }

    public function deleteTime($id)
    {
        if(DB::table('available_time')->where('id', $id)->delete()){
            return response()->json("delete successfully",200);
        }
        else{
            return response()->json("Can not delete");
        }
    }
}

?>