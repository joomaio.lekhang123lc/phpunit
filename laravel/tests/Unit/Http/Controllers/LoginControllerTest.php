<?php

//namespace Tests\Unit\Repositories;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Hash;
use DB;

class LoginControllerTest extends TestCase
{
    protected $db;

    protected $user;

    public function setUp() : void
    {
        $this->afterApplicationCreated(function () {
            $this->user = [
                'name'     => 'demo',
                'username' => 'demo',
                'password' => Hash::make('123123'),
                'created_at'=>now(),
                'updated_at'=>now()
                //'role'     => 0,
                //'email'    => 'user@gmail.com'
            ];
            // insert user
            DB::table('user')->insert($this->user);
                
        });
        parent::setUp();
    }

    public function tearDown() : void
    {
        // delete created user
        DB::table('user')->where('username','=',$this->user['username'])->delete();
        parent::tearDown();
    }

    /**
     * A basic unit test login.
     *
     * @return void
     */
    public function testLoginSuccess() : string
    {
        $loginController = new LoginController;
        request()->merge([
            'name'     => 'demo',
            'username' => 'demo',
            'password' => '123123'
        ]);

        $response = $loginController->authenticate(request());
        $this->assertArrayHasKey('token', $response->original);
        return $response->original['token'];
    }

    /**
     * @depends testLoginSuccess
     */
    public function testCreatedSession(string $token){
        $this->assertArrayHasKey($token, $_SESSION);
    }

    // public function testLoginFailed() : void
    // {
    //     $loginController = new LoginController;
    //     request()->merge([
    //         'name'     => 'demo',
    //         'username' => 'demo',
    //         'password' => 'nothing'
    //     ]);

    //     $response = $loginController->authenticate(request());
    //     $this->assertArrayHasKey('token', $response->original);
    //     //return $response->original['token'];
    // }
}
?>